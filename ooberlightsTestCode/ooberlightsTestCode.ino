// This should probably have a license


#include <stdint.h>

#include <Adafruit_NeoPixel.h>


int MAXBRIGHT = 60; //20 to 150?


// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS 45

Adafruit_NeoPixel ring_left(NUMPIXELS, 5, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel ring_right(NUMPIXELS, 4, NEO_GRB + NEO_KHZ800);


#define DELAYVAL 20 // Time (in milliseconds) to pause between pixels

#include <ESP8266WiFi.h>
#include "wificredentials.h"

WiFiServer server(80);

unsigned int loop_count = 0;
int demo = 2;

uint32_t rgb_mask = 0x00ffffff;
uint32_t brightness_mask = 0x007f7f7f;
uint32_t array[90], current;
int pos = 0;


void setup() {
  randomSeed(analogRead(A0));

  WiFi.begin(ssid, password);

  server.begin();
  Serial.println("Server started");

  for (int i = 0; i < 90; ++i) {
    array[i] = 0;
  }
  ring_right.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
  ring_left.begin();

}

void tempWifi() {
  // Check if a client has connected
  WiFiClient client = server.available();
  if (!client) {
    return;
  }

  // Wait until the client sends some data
  Serial.println("new client");
  while (!client.available()) {
    delay(1);
  }

  // Read the first line of the request
  String req = client.readStringUntil('\r');
  Serial.println(req);
  client.flush();

  int val;

  val = req.substring(12).toInt();

  if (val > 1000) {
    MAXBRIGHT = val - 1000;
    if (MAXBRIGHT > 150) {
      MAXBRIGHT = 150;
    }
  } else {
    demo = val;
  }

  client.flush();

  // Prepare the response
  String s = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n<!DOCTYPE HTML>\r\n<html>\r\n";
  s +=  "<title>OoberLights Test Patterns</title><body>";
  s +=  "<h1>ObberLights Test Patterns</h1>";
  s +=  "<ul>";
  s +=  "<li><a href=\"/switch/0\">Brightness Test</a></li>";
  s +=  "<li><a href=\"/switch/1\">Brightness Compare</a></li>";
  s +=  "<li><a href=\"/switch/2\">Twinkle</a></li>";
  s +=  "<li><a href=\"/switch/4\">CUDyin A</a></li>";
  s +=  "<li><a href=\"/switch/3\">CUDyin B</a></li>";
  s +=  "<li><a href=\"/switch/5\">CUDyin C</a></li>";
  s +=  "<li><a href=\"/switch/6\">Spinner A</a></li>";
  s +=  "<li><a href=\"/switch/7\">Spinner B</a></li>";
  s +=  "<li><a href=\"/switch/8\">Spinner C</a></li>";
  s +=  "<li><a href=\"/switch/1020\">Set Brightness Low</a></li>";
  s +=  "<li><a href=\"/switch/1090\">Set Brightness Med</a></li>";
  s +=  "<li><a href=\"/switch/1150\">Set Brightness High</a></li>";

  s += "</ul><p></body></html>\n";

  // Send the response to the client
  client.print(s);
  delay(1);
  Serial.println("Client disconnected");

  // The client will actually be disconnected
  // when the function returns and 'client' object is detroyed

}

void spinner(int direction, int rpm, int r, int g, int b, int length) {

  int delay = 42 * rpm;

  int pixel = 21;

}

void twinkle() {

  for (int i = 0; i < NUMPIXELS; i++) { // For each pixel...


    ring_right.setPixelColor(44 - i, ring_right.Color(random(0, MAXBRIGHT), random(0, MAXBRIGHT), random(0, MAXBRIGHT)));

    ring_right.show();   // Send the updated pixel colors to the hardware.

    ring_left.setPixelColor(i, ring_left.Color(random(0, MAXBRIGHT), random(0, MAXBRIGHT), random(0, MAXBRIGHT)));
    ring_left.show();   // Send the updated pixel colors to the hardware.

    delay(DELAYVAL); // Pause before next pass through loop
  }


}

void brightness_compare() {
  ring_right.clear(); // Set all pixel colors to 'off'
  ring_left.clear();

  for (int i = 0; i < NUMPIXELS; i++) { // For each pixel...

    ring_right.setPixelColor(i, ring_right.Color(i * 5, i * 5, i * 5));

    ring_right.show();   // Send the updated pixel colors to the hardware.

    ring_left.setPixelColor(i, ring_right.Color(i * 5, i * 5, i * 5));
    ring_left.show();   // Send the updated pixel colors to the hardware.

    delay(DELAYVAL); // Pause before next pass through loop
  }
  delay(3000);

}

void maxbright_test() {

  for (int i = 0; i < NUMPIXELS; i++) { // For each pixel...


    ring_right.setPixelColor(i, ring_right.Color(MAXBRIGHT, MAXBRIGHT, MAXBRIGHT));

    ring_right.show();   // Send the updated pixel colors to the hardware.

    ring_left.setPixelColor(i, ring_left.Color(MAXBRIGHT, MAXBRIGHT, MAXBRIGHT));
    ring_left.show();   // Send the updated pixel colors to the hardware.

    delay(DELAYVAL); // Pause before next pass through loop
  }
  delay(3000);

}

void dyinA() {

  ///WARNING: this is a stress test for update/refresh frequencies -- use at your own peril.
  for (int i = 0; i < NUMPIXELS; ++i) {
    //if (i+1) divides NUMPIXELS
    if (0 == (loop_count % (i + 1))) {
      int r = random(0, MAXBRIGHT + 1);
      int g = random(0, MAXBRIGHT + 1);
      int b = random(0, MAXBRIGHT + 1);
      ring_right.setPixelColor(i, ring_right.Color(r, g, b));
    }
    if (0 == (loop_count % (45 + i + 1))) {
      int r = random(0, MAXBRIGHT + 1);
      int g = random(0, MAXBRIGHT + 1);
      int b = random(0, MAXBRIGHT + 1);
      ring_left.setPixelColor(i, ring_left.Color(r, g, b));
    }
  }
  loop_count += 1;
  ring_right.show();
  ring_left.show();
  // no delay on purpose


}

void dyinB() {

  for (int i = 0; i < NUMPIXELS; i++) {
    float time = (float)millis();
    int r = MAXBRIGHT * 0.5 * (1 + sin((float)time + (float)i / NUMPIXELS));
    int g = MAXBRIGHT * 0.5 * (1 + cos((float)time + (float)i / NUMPIXELS));
    int b = MAXBRIGHT * 0.5 * (1 + tan((float)time + (float)i / NUMPIXELS));
    ring_left.setPixelColor(i, ring_left.Color(r, g, b));

    r = MAXBRIGHT * 0.5 * (1 + sin((float)(i + loop_count + 0)));
    g = MAXBRIGHT * 0.5 * (1 + cos((float)(i + loop_count + 1)));
    b = MAXBRIGHT * 0.5 * (1 + tan((float)(i + loop_count + 2)));
    ring_right.setPixelColor(i, ring_right.Color(r, g, b));
  }
  loop_count += 1;
  ring_right.show();
  ring_left.show();
  delay(30); // 30 ms delay = about 30 fps;

}

void dyinC() {
  if (0 == pos) {
    current = random(0, rgb_mask + 1) & brightness_mask;
    delay(10);
  } else {
    array[pos] += current / 0x00080808 - (pos % 45) * 0x00010101;
    array[pos] = (array[pos] & rgb_mask) & brightness_mask;
  }

  for (int i = 0; i < 45; ++i) {
    if (i != pos) {
      ring_right.setPixelColor(i, array[i]);
    } else {
      ring_right.setPixelColor(i, current);
    }
    if (i + 45 != pos) {
      ring_left.setPixelColor(i, array[i + 45]);
    } else {
      ring_left.setPixelColor(i, current);
    }
    ring_right.show();
    ring_left.show();
  }

  pos = (pos + 1) % 90;

}

void loop() {
  //  demo=random(0,5);

  while (1) {
    tempWifi();
    switch (demo) {
      case 0:
        maxbright_test();
        break;
      case 1:
        brightness_compare();
        break;
      case 2:
        twinkle();
        break;
      case 3:
        dyinB();
        break;
      case 4:
        dyinA();
        break;
      case 5:
        dyinC();
        break;
      case 6:
        spinner(1, 60, 50, 50, 0, 2);
      case 7:
        spinner(1, 120, 50, 0, 50, 4);
      case 8:
        spinner(0, 60, 0, 50, 50, 1);
        break;
      default:
        twinkle();
    }
  }
}
