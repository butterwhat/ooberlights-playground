// This should probably have a license

#include <stdint.h>
#include <Adafruit_NeoPixel.h>

const uint32_t NO_WHITE_MASK = 0x00ffffff, NO_RED_MASK = 0xff00ffff,
               NO_GREEN_MASK = 0xffff00ff, NO_BLUE_MASK = 0xffffff00;

const uint32_t COLOR_CHANNEL_MASK = 0x000000ff;

const size_t WHITE_SHIFT = 24, RED_SHIFT = 16, GREEN_SHIFT = 8, BLUE_SHIFT = 0;

int MAXBRIGHT=60;  //20 to 150?

#define DELAYVAL 20 // Time (in milliseconds) to pause between pixels

#include <ESP8266WiFi.h>
#include "wificredentials.h"

WiFiServer server(80);

unsigned int loop_count = 0;
int demo=6;

unsigned long ms;


// Array for the whole disc (disc isn't better, right?!) and Ring for each circle?
class Array {
  int direction; // -1 or 1, prototypes spin opposite directions!
  Adafruit_NeoPixel pixels;

  void setPixels(int pin) {
    pixels.setPin(pin);
    pixels.updateLength(NUMPIXELS);
    pixels.updateType(NEO_GRB + NEO_KHZ800);
  };
  
};

class PixelRef {
  Adafruit_NeoPixel &neopixels;
  const size_t index;

private:
  uint8_t getPixelPart(const size_t& shift) const {
    return static_cast<uint8_t>((neopixels.getPixelColor(index) >> shift) & COLOR_CHANNEL_MASK);
  }
  void setPixelPart(const uint8_t &value, const uint32_t &mask,
                    const size_t &shift) {
    neopixels.setPixelColor(index, (neopixels.getPixelColor(index) & mask) |
                                       (static_cast<uint32_t>(value) << shift));
  }

public:
  //! constructors
  PixelRef(Adafruit_NeoPixel &pixels, const size_t idx)
      : neopixels(pixels), index(idx) {}
  //! getters
  uint8_t red() const { return getPixelPart(RED_SHIFT); }
  uint8_t green() const { return getPixelPart(GREEN_SHIFT); }
  uint8_t blue() const { return getPixelPart(BLUE_SHIFT); }
  //! setters
  PixelRef &red(const uint8_t &value) {
    setPixelPart(value, NO_RED_MASK, RED_SHIFT);
    return *this;
  }
  PixelRef &green(const uint8_t &value) {
    setPixelPart(value, NO_GREEN_MASK, GREEN_SHIFT);
    return *this;
  }
  PixelRef &blue(const uint8_t &value) {
    setPixelPart(value, NO_BLUE_MASK, BLUE_SHIFT);
    return *this;
  }
};

class Slice {
  Adafruit_NeoPixel neopixels;

public:
  PixelRef pixelFromPolar(int32_t angle_centidegrees, int ring) {
    return PixelRef(pixel_index);
  }
};

class Goober {
  int id;
};

class Spinner : public Goober {
  int ring; // 0 == center pixel, 3 == outer ring?
  int length;  // length in degrees or pixels?  degrees I say!
  int r, g, b;
  int rpm;
  void draw();
};

void Spinner::draw() {
  // outer ring == 21 to 44

  int i= ((ms/rpm) % 24) + 21;
  
  ring_right.setPixelColor(i, ring_right.Color(r, g, b));

}


void setup() {
  randomSeed(analogRead(A0));

  WiFi.begin(ssid, password);

  server.begin();
  Serial.println("Server started");


  ring_right.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
  ring_left.begin();

}

void tempWifi() {
  // Check if a client has connected
  WiFiClient client = server.available();
  if (!client) {
    return;
  }
  
  // Wait until the client sends some data
  Serial.println("new client");
  while(!client.available()){
    delay(1);
  }
  
  // Read the first line of the request
  String req = client.readStringUntil('\r');
  Serial.println(req);
  client.flush();

  int val;

  val=req.substring(12).toInt();

  if (val > 1000) { MAXBRIGHT=val-1000; if (MAXBRIGHT > 150) { MAXBRIGHT=150;} } else { demo=val; }

  client.flush();

  // Prepare the response
  String s = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n<!DOCTYPE HTML>\r\n<html>\r\n";
  s+=  "<title>OoberLights Test Patterns</title><body>";
  s+=  "<h1>ObberLights Test Patterns</h1>";
  s+=  "<ul>";
  s+=  "<li><a href=\"/switch/0\">Brightness Test</a></li>";
  s+=  "<li><a href=\"/switch/1\">Brightness Compare</a></li>";
  s+=  "<li><a href=\"/switch/2\">Twinkle</a></li>";
  s+=  "<li><a href=\"/switch/4\">CUDyin A</a></li>";
  s+=  "<li><a href=\"/switch/3\">CUDyin B</a></li>";
  s+=  "<li><a href=\"/switch/5\">CUDyin C</a></li>";
  s+=  "<li><a href=\"/switch/6\">Spinner A</a></li>";
  s+=  "<li><a href=\"/switch/7\">Spinner B</a></li>";
  s+=  "<li><a href=\"/switch/8\">Spinner C</a></li>";
  s+=  "<li><a href=\"/switch/1020\">Set Brightness Low</a></li>";
  s+=  "<li><a href=\"/switch/1090\">Set Brightness Med</a></li>";
  s+=  "<li><a href=\"/switch/1150\">Set Brightness High</a></li>";

  s += "</ul><p></body></html>\n";

  // Send the response to the client
  client.print(s);
  delay(1);
  Serial.println("Client disconnected");

  // The client will actually be disconnected 
  // when the function returns and 'client' object is detroyed
  
}

void loop() {
  //  demo=random(0,5);

  while(1) {
    ms=millis();  // Make sure all Goobers are in sync
    tempWifi();
  }
}
