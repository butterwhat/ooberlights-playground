  /*! "If the frequency of the crystal oscillator is 40 MHz, then the baud rate
     for printing is 115200; if the frequency of the crystal oscillator is 26
     MHz, then the baud rate for printing is 74880. "
                       -- ESP8266EX datasheet, remark preceding section 4.7

     and the crystal on the wroom is indeed 26 MHz, but 74880 fails with the
     arduino 1.8.12 IDE as GNU coreutils's stty doesn't support that baud rate
     in version 8.32 . 57600 is a standard rate that works, but the ESP is
     outputting boot 'stuff' at 74880.

     If you're on a sytem with GNU coreutils, and must know the 'boring' parts
     -- i.e. must use 74880 -- then you can compile the tools/anybaud.c to set
     it to 74880. Do note however that this has to be re-set every time the baud
     rate on that interface changes. That is, on connect, after firmware/sketch
     upload, etc.
  */
  Serial.begin(
    57600);  // if you read the above comments, then you can set this to 74880
  Serial.setDebugOutput(false);
  if ( !ESP.checkFlashCRC() ) {
    Serial.println("corrupted program flash detected");
  }
  Serial.println("Booting Sketch...");

  serial_input_buffer = new char[serial_input_buffer_size];
  memset(serial_input_buffer, '\0', serial_input_buffer_size);
