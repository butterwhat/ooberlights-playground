#pragma once
#ifndef ANIMATION_API_H
#define ANIMATION_API_H

#ifndef NO_ARDUINO
#include <Adafruit_NeoPixel.h>
#else
#include <cmath>
#include <cstdio>
#include <cinttypes>
#include <cstdint>
#include <cstddef>
#include <cstring>
struct Adafruit_NeoPixel {
  size_t             numPixels() const { return 42; }
  Adafruit_NeoPixel &clear() { return *this; }
  static uint32_t
  Color(const uint8_t, const uint8_t, const uint8_t, const uint8_t = 0) {
    return 0x00ffcc88;
  }
  static uint32_t
  ColorHSV(const uint8_t, const uint8_t, const uint8_t, const uint8_t = 0) {
    return 0x00ffcc88;
  }
  static uint8_t     sine8(uint8_t) { return 42; }
  Adafruit_NeoPixel &setPixelColor(const size_t, const uint32_t) {
    return *this;
  }
  Adafruit_NeoPixel &show() { return *this; }
};

int32_t random(int32_t from, int32_t to);

struct Stream {
    void printf(...) {}
    void println(...) {}
};
#endif

struct AnimationOption {
  const char *  name;
  const char *  description;
  const int32_t min;
  const int32_t max;
  const int32_t default_value;
};

typedef void *(*animationSetupFptr)(Adafruit_NeoPixel **, uint8_t num_rings);
typedef void (*animationTeardownFptr)(void *);
typedef void (*animationStepFptr)(void *);
typedef void (*animationSetOptionFptr)(void *,
                                       const char *   option_name,
                                       const uint32_t value);
typedef void (*animationGetOptionFptr)(const AnimationOption **optionInfo,
                                       size_t *                n);

struct Animation {
  const char *           name;
  animationSetupFptr     setupFunc;
  animationTeardownFptr  teardownFunc;
  animationStepFptr      stepFunc;
  animationSetOptionFptr setOptFunc;
  animationGetOptionFptr getOptionsFunc;
};

struct AnimationWithState {
  Animation *animation = nullptr;
  void *     ctx       = nullptr;
  explicit AnimationWithState(Animation *a, void *c) : animation(a), ctx(c) {}
  AnimationWithState(const AnimationWithState &that)
      : animation(that.animation), ctx(that.ctx) {}
};


Animation *getAnimationByName(const char *name);

const char *getAnimationNameFromStepFunc(animationStepFptr stepFunc);

/** get the name for a given \p Animation.
 *
 * @return the animation name if anim is a valid Animation instance, \p nullptr
 * otherwise.
 */
const char *getAnimationName(Animation *anim);


/** print all options for a given animation.
 *
 * @param[out]  out   The \p Stream to print the options to.
 * @param[in]   anim  The \p Animation to print the options thereof.
 */
void printAnimationOptions(Stream &out, Animation const *const anim);

#endif
