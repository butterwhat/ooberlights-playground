#pragma once

#include "../util/memory.h"

struct OneAtATimeContext {
  Adafruit_NeoPixel **rings;
  int *               positions;
  uint8_t             num_rings;
};

void *oaatAnimationSetup(Adafruit_NeoPixel **rings, uint8_t num_rings) {
  OneAtATimeContext *result = NEW(OneAtATimeContext);

  if ( nullptr != result ) {
    result->rings     = rings;
    result->num_rings = num_rings;
    result->positions = NEW(int[num_rings]);
    if ( nullptr == result->positions ) {
      DELETE(result);
      return nullptr;
    }
    for ( int k = 0; k < num_rings; ++k ) {
      if ( nullptr != rings[k] ) {
        result->positions[k] = random(0, rings[k]->numPixels());
      }
    }
  }

  return result;
}

void oaatAnimationTeardown(void *ctx) {
  OneAtATimeContext *c = (OneAtATimeContext *)(ctx);
  if ( nullptr == c ) {
    return;
  }
  DELETE(c);
}

void oaatAnimationStep(void *ctx) {
  OneAtATimeContext *c = (OneAtATimeContext *)(ctx);
  if ( nullptr == c ) {
    return;
  }
  for ( int k = 0; k < c->num_rings; ++k ) {
    if ( nullptr != c->rings[k] ) {
      c->rings[k]->clear();
      c->rings[k]->setPixelColor(c->positions[k] % c->rings[k]->numPixels(),
                                 c->rings[k]->Color(random(0, 0x10),
                                                    random(0, 0x10),
                                                    random(0, 0x10)));
      c->rings[k]->show();
      c->positions[k] = (c->positions[k] + 1) % c->rings[k]->numPixels();
    }
  }
}
