#pragma once
#include "../util/angle.h"
#include "../util/string.h"

struct ScanlineContext {
  alignas(4) Adafruit_NeoPixel **rings;
  alignas(4) uint32_t            color : 24;
  alignas(4) uint32_t            num_rings : 7;
  alignas(4) uint32_t            cycle_colors : 1;
  alignas(2) int16_t             angle;
  alignas(2) int16_t             color_drag;
  alignas(1) uint8_t             counter;
};

void *scanlineAnimationSetup(Adafruit_NeoPixel **rings, uint8_t num_rings) {
  ScanlineContext *result = NEW(ScanlineContext);

  if ( nullptr != result ) {
    result->rings        = rings;
    result->num_rings    = num_rings;
    result->angle        = INT16_C(0);
    result->color        = UINT32_C(0x000800);
    result->cycle_colors = UINT32_C(1);
    result->color_drag   = INT16_C(0);
    result->counter      = 0;
  }

  return result;
}

void scanlineAnimationTeardown(void *ctx) {
  ScanlineContext *c = (ScanlineContext *)(ctx);
  if ( nullptr != c ) {
    DELETE(c);
  }
}

void scanlineAnimationSetOption(void *         ctx,
                                const char *   option_name,
                                const uint32_t value) {
  if ( nullptr == ctx || nullptr == option_name ) {
    return;
  }
  ScanlineContext *c = (ScanlineContext *)(ctx);
  STRING_BEGINS_WITH(option_name, "initial_angle") {
    c->angle = (uint16_t)(value);
  }
  OR_WITH(option_name, "cycle_colors") { c->cycle_colors = value; }
  OR_WITH(option_name, "color") { c->color = value; }
}

static const uint32_t color1 = Adafruit_NeoPixel::Color(0, 0x08, 0);
static const uint32_t color2 = Adafruit_NeoPixel::Color(0, 0x06, 0);
static const uint32_t color3 = Adafruit_NeoPixel::Color(0, 0x04, 0);

void scanlineAnimationStep(void *ctx) {
  ScanlineContext *c = (ScanlineContext *)(ctx);
  int              led;
  if ( nullptr == c || nullptr == c->rings ) {
    return;
  }
  for ( int k = 0; k < c->num_rings; ++k ) {
    // the api maps 360 degrees onto all values from 0 to 65536
    uint32_t color = Adafruit_NeoPixel::ColorHSV(
        UINT16_C(182) * (c->angle + c->color_drag), 255,
        Adafruit_NeoPixel::sine8(c->counter++) / 7);
    if ( nullptr == c->rings[k] ) {
      continue;
    }
    c->rings[k]->clear();
    for ( int ell = 0; ell < 4; ++ell ) {
      /*
      led = led_for_angle(ell, c->angle * 100);
      if(UINT16_MAX == led) {
          continue;
      }
      c->rings[k]->setPixelColor(led, color3);

      led = led_for_angle(ell, ((c->angle + 10)%360) * 100);
      if(UINT16_MAX == led) {
          continue;
      }
      c->rings[k]->setPixelColor(led, color2);
      */

      led = led_for_angle(ell, ((c->angle + 20) % 360) * 100);
      if ( UINT16_MAX == led ) {
        continue;
      }
      c->rings[k]->setPixelColor(led, color);
    }
    c->rings[k]->show();
  }
  c->angle += 10;
  c->angle %= 360;
  c->color_drag -= 3;
}

static const AnimationOption options[] = {
    {"initial_angle", "starting angle for scanline (degrees)", 0, 359, 0},
    {"cycle_colors",
     "bool to indicate if colors should be cycled through HSL space (true) or "
     "not(false)",
     0, 1, 1},
    {"color", "set color for the monochromatic scanline", 0, 0x00ffffff,
     0x00001000}};

void scanlineAnimationGetOptions(const AnimationOption **optionInfo,
                                size_t            *n) {
    if(nullptr != optionInfo && nullptr != n) {
        *optionInfo = options;
        *n = sizeof(options)/sizeof(options[0]);
    }
}
