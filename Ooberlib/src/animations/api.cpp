#include "api.h"

#include "ooat.h"
#include "scanline.h"
#include "twinkle.h"

Animation animations[] = {
    {"one at a time", oaatAnimationSetup, oaatAnimationTeardown,
     oaatAnimationStep, nullptr, nullptr},
    {"scanline", scanlineAnimationSetup, scanlineAnimationTeardown,
     scanlineAnimationStep, nullptr, scanlineAnimationGetOptions},
    {"twinkle", twinkleAnimationSetup, twinkleAnimationTeardown,
     TwinkleAnimationStep, nullptr, nullptr},
    {nullptr, nullptr, nullptr, nullptr, nullptr,
     nullptr}  // must be last entry
};

Animation *getAnimationByName(const char *name) {
  Animation *ptr = animations + 0;
  for ( ; nullptr != ptr->name; ++ptr ) {
    if ( !strcmp(ptr->name, name) ) {
      break;
    }
  }
  return ptr;
}

const char *getAnimationNameFromStepFunc(animationStepFptr stepFunc) {
  Animation *ptr = animations + 0;
  for ( ; nullptr != ptr->name; ++ptr ) {
    if ( stepFunc == ptr->stepFunc ) {
      return ptr->name;
    }
  }
  return nullptr;
}

/** get the name for a given \p Animation.
 *
 * @return the animation name if anim is a valid Animation instance, \p nullptr
 * otherwise.
 */
const char *getAnimationName(Animation *anim) {
  if ( nullptr == anim ) {
    return nullptr;
  }
  return getAnimationNameFromStepFunc(anim->stepFunc);
}

static inline size_t field_length(const char *str) { return strlen(str); }

static inline size_t field_length(int32_t val) {
  bool negative = 0 > val;
  if ( negative ) {
    if ( INT32_MIN == val ) {
      val = INT32_MAX;
    } else {
      val = -val;
    }
  }
  if ( INT32_C(0) == val ) {
    return 1;
  }
  return (size_t)(log10(val)) + 1 + (size_t)negative;
}

static inline size_t max(const size_t &a, const size_t &b) {
  if ( a >= b ) {
    return a;
  }
  return b;
}

/** print all options for a given animation.
 *
 * @param[out]  out   The \p Stream to print the options to.
 * @param[in]   anim  The \p Animation to print the options thereof.
 */
void printAnimationOptions(Stream &out, Animation const *const anim) {
  const AnimationOption *animOpts;
  size_t                 animOptionCount = 0;
  anim->getOptionsFunc(&animOpts, &animOptionCount);

  size_t widths[5]{0, 0, 0, 0, 0};

  for ( size_t opt = 0; opt < animOptionCount; ++opt ) {
    widths[0] = max(widths[0], field_length(animOpts[opt].name));
    widths[1] = max(widths[1], field_length(animOpts[opt].description));
    widths[2] = max(widths[2], field_length(animOpts[opt].default_value));
  }

  char fmt_string[256];
  snprintf(fmt_string, 256,
           "%%-%us | %%-%us | %%-%u" PRIi32 " [%%" PRIi32 " , %%" PRIi32 "]\n",
           widths[0], widths[1], widths[2]);

  out.printf("----- options for animation '%s' -----\n",
             getAnimationName((Animation *)anim));
  for ( size_t opt = 0; opt < animOptionCount; ++opt ) {
    out.printf(fmt_string, animOpts[opt].name, animOpts[opt].description,
               animOpts[opt].default_value, animOpts[opt].min,
               animOpts[opt].max);
  }
  out.println("------------------------------------");
}


