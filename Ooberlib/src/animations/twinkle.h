#pragma once
struct TwinkleAnimationContext {
  Adafruit_NeoPixel **rings;
  uint32_t            counter;
  uint8_t             num_rings;
};

void *twinkleAnimationSetup(Adafruit_NeoPixel **rings, uint8_t num_rings) {
  TwinkleAnimationContext *result = NEW(TwinkleAnimationContext);
  if ( nullptr != result ) {
    result->rings     = rings;
    result->counter   = UINT32_C(0);
    result->num_rings = num_rings;
  }
  return result;
}

void twinkleAnimationTeardown(void *ctx) {
  if ( nullptr != ctx ) {
    TwinkleAnimationContext *c = (TwinkleAnimationContext *)(ctx);
    DELETE(c);
  }
}

// speed :  5375 18
//        17105 53
//        22161 68
// = ~335 iterations / s
void TwinkleAnimationStep(void *ctx) {
  static int i = 0;
  static int k = 0;
  if ( nullptr == ctx ) {
    return;
  }

  TwinkleAnimationContext *c = (TwinkleAnimationContext *)(ctx);

  for ( k = 0; k < c->num_rings; ++k ) {
    auto ring = c->rings[k];
    if ( nullptr == ring ) {
      continue;
    }
    for ( i = 0; i < c->rings[k]->numPixels(); ++i ) {
      if ( 0 == c->counter % (k * ring->numPixels() + i + 1) ) {
        ring->setPixelColor(i,
                            c->rings[k]->Color(random(0, 0x2f), random(0, 0x2f),
                                               random(0, 0x2f)));
      }
    }
  }
  c->counter += 1;
  for ( k = 0; k < c->num_rings; ++k ) {
    if ( nullptr == c->rings[k] ) {
      continue;
    }
    c->rings[k]->show();
  }
}
