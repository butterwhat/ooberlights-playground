  // MDNS.begin(hostname);

  httpServer  = new ESP8266WebServer(80);
  httpUpdater = new ESP8266HTTPUpdateServer();

  httpUpdater->setup(httpServer);
  httpServer->begin();

  httpServer->on("/", handleHttpIndex);
  // json requests for leds

  httpServer->on("/leds", HTTP_POST, replyOk, handleHttpLeds);
  httpServer->on("/ledstatus", handleHttpLedStatus);

  // MDNS.addService("http", "tcp", 80);
  Serial.printf(
    "HTTPUpdateServer ready! Open http://%s/update in your browser\n",
    WiFi.localIP().toString().c_str());
  Serial.flush();
