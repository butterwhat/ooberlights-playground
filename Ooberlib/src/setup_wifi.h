void setupWifi() {
  WiFi.hostname(hostname + String(ESP.getChipId(), HEX));
  if ( 0 != wifi_output_power_decis % 25 ) {
    Serial.println(
        "wifi output power decis setting not a multiple of 25, rounding down.");
  }
  WiFi.setOutputPower(
      (float)(wifi_output_power_decis - wifi_output_power_decis % 25) / 10.f);
  delay(10);

  WiFi.softAPConfig(soft_ap_address, soft_ap_gateway_address, soft_ap_subnet_mask);

  if ( !WiFi.softAP(wifi_ssid, wifi_password, wifi_channel, wifi_hide_ssid,
                    wifi_max_connections) ) {
    Serial.println("failed to create soft-AP");
    }

  WiFi.printDiag(Serial);
}
