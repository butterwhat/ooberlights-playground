#pragma once
#ifndef TEMPLATED_HELPERS_HPP
#define TEMPLATED_HELPERS_HPP

#include "util/memory.h"

namespace {
AnimationWithState *make_animation(Adafruit_NeoPixel **rings,
                                   const uint8_t       num_rings,
                                   const char *        name);

CompositorWithState *make_compositor(Adafruit_NeoPixel **rings,
                                     const uint8_t       num_rings,
                                     const char *        name);

template<typename C> C *get_by_name(const char *name) {
  (void)name;
  return nullptr;
}

template<> Animation *get_by_name<Animation>(const char *name) {
  return getAnimationByName(name);
}

template<> Compositor *get_by_name<Compositor>(const char *name) {
  return getCompositorByName(name);
}

template<typename C, typename S>
S *make_stateful(Adafruit_NeoPixel **rings,
                 const uint8_t       num_rings,
                 const char *        name) {
  C *stateless = get_by_name<C>(name);

  if ( nullptr == stateless || nullptr == stateless->setupFunc ) {
    return nullptr;
  }

  void *ctx = stateless->setupFunc(rings, num_rings);
  S *   res = NEW(S(stateless, ctx));

  if ( nullptr == res ) {
    if ( nullptr != stateless->teardownFunc ) {
      stateless->teardownFunc(ctx);
    }
    return nullptr;
  }

  return res;
}

AnimationWithState *make_animation(Adafruit_NeoPixel **rings,
                                   const uint8_t       num_rings,
                                   const char *        name) {
  return make_stateful<Animation, AnimationWithState>(rings, num_rings, name);
}

CompositorWithState *make_compositor(Adafruit_NeoPixel **rings,
                                     const uint8_t       num_rings,
                                     const char *        name) {
  return make_stateful<Compositor, CompositorWithState>(rings, num_rings, name);
}
}  // namespace

#endif /* TEMPLATED_HELPERS_HPP */
