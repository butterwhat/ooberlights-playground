#include "api.h"

#ifndef NO_ARDUINO
//#include "ghost.h"
#include "passthrough.h"
#endif

static Compositor compositors[] = {
#if 0
    {"ghost", ghostCompositorSetup, ghostCompositorTeardown,
     ghostCompositorStep, ghostCompositorAddAnimation,
     ghostCompositorAddSubCompositor},
#endif
#ifndef NO_ARDUINO
    {"passthrough", passthroughCompositorSetup, passthroughCompositorTeardown,
     passthroughCompositorStep, passthroughCompositorAddAnimation, nullptr, nullptr, nullptr},
    {nullptr, nullptr, nullptr, nullptr, nullptr,
     nullptr, nullptr, nullptr}  // must be last entry
#endif
};

Compositor *getCompositorByName(const char *name) {
  Compositor *ptr = compositors + 0;
  for ( ; nullptr != ptr->name; ++ptr ) {
    if ( !strcmp(ptr->name, name) ) {
      break;
    }
  }
  return ptr;
}

const char *getCompositorName(Compositor *comp) {
  if ( nullptr == comp ) {
    return nullptr;
  }
  return comp->name;
}

const char *getCompositorNameFromStepFunc(compositorStepFptr stepFunc) {
  Compositor *ptr = compositors + 0;
  for ( ; nullptr != ptr->name; ++ptr ) {
    if ( stepFunc == ptr->stepFunc ) {
      break;
    }
  }
  return getCompositorName(ptr);
}
