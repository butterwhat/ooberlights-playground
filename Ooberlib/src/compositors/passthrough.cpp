#include "passthrough.h"
#include "../util/memory.h"

struct PassthroughCompositorContext {
  Adafruit_NeoPixel **rings;
  uint8_t             num_rings;
  AnimationWithState *animation;
};

static PassthroughCompositorContext *voidToCtx(void *ctx) {
  return (PassthroughCompositorContext *)(ctx);
}

void *passthroughCompositorSetup(Adafruit_NeoPixel **rings, uint8_t num_rings) {
  PassthroughCompositorContext *result = NEW(PassthroughCompositorContext);
  if ( nullptr != result ) {
    result->rings     = rings;
    result->num_rings = num_rings;
    result->animation = nullptr;
  }

  return result;
}

void passthroughCompositorTeardown(void *ctx) {
  if ( nullptr == ctx ) {
    return;
  }
  PassthroughCompositorContext *c = voidToCtx(ctx);
  if ( nullptr != c->animation && nullptr != c->animation->animation &&
       nullptr != c->animation->animation->teardownFunc ) {
    c->animation->animation->teardownFunc(c->animation->ctx);
    DELETE(c->animation);
  }
  DELETE(c);
}

void passthroughCompositorAddAnimation(void *              ctx,
                                       AnimationWithState *animwstate) {
  if ( nullptr == ctx ) {
    Serial.println("can't add animation to stateless compositor");
    Serial.flush();
    return;
  }
  PassthroughCompositorContext *c = voidToCtx(ctx);
  // tear down previous animation if any
  if ( nullptr != c->animation && nullptr != c->animation->animation &&
       nullptr != c->animation->animation->teardownFunc ) {
    c->animation->animation->teardownFunc(c->animation->ctx);
  }
  c->animation = animwstate;
  Serial.printf("anim add: %p\n", animwstate);
  Serial.flush();
}

void passthroughCompositorStep(void *ctx) {
  if ( nullptr == ctx ) {
    return;
  }
  PassthroughCompositorContext *c = voidToCtx(ctx);

  if ( nullptr != c->animation && nullptr != c->animation->animation &&
       nullptr != c->animation->animation->stepFunc ) {
    c->animation->animation->stepFunc(c->animation->ctx);
#ifdef OOBERDEBUG
    Serial.println("s");
    Serial.flush();
#endif
  } else {
#ifdef OOBERDEBUG
    if ( nullptr == c->animation ) {
      Serial.println("animation is null!");
    } else if ( nullptr == c->animation->animation ) {
      Serial.println("animation's animation struct is null!?");
    } else if ( nullptr == c->animation->animation->stepFunc ) {
      Serial.println("animation without a step function? that won't work!");
    } else {
      Serial.println("My name's Heisenbug");
    }
    Serial.flush();
#endif
  }
}
