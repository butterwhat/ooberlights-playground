#pragma once
#ifndef COMPOSITOR_GHOST_H
#define COMPOSITOR_GHOST_H

struct GhostCompositorContext {
  Adafruit_NeoPixel **drawingCtxs;  // actual rings
  Adafruit_NeoPixel * sourceCtxs;  // virtual surfaces for underlying animations
  uint8_t             num_rings;
  uint8_t             num_ghosts;
  uint8_t **          ghost_buffers;
};

void ghostCompositorTeardown(void *ctx) {
  GhostCompositorContext *c = (GhostCompositorContext *)(ctx);
  if ( nullptr == c ) {
    return;
  }
  if ( nullptr != c->sourceCtxs ) {
    delete[] c->sourceCtxs;
  }

  if ( nullptr != ghost_buffers ) {
    for ( uint8_t i = 0; i < num_rings; ++i ) {
      if ( nullptr != c->ghost_buffers[i] ) {
        delete[] c->ghost_buffers[i];
      }
    }
    delete[] c->ghost_buffers;
  }
  delete c;
}

void *ghostCompositorSetup(Adafruit_NeoPixel **rings, uint8_t num_rings) {
  GhostCompositorContext *result = new GhostCompositorContext;
  if ( nullptr != result ) {
    result->drawingCtxs = rings;
    result->num_ghosts  = 4;
    result->sourceCtxs  = new Adafruit_NeoPixel[num_rings];
    if ( nullptr == result->sourceCtxs ) {
      ghostCompositorTeardown(result);
      return nullptr;
    }

    ghost_buffers = new uint8_t *[num_rings];
    if ( nullptr == ghost_buffers ) {
      ghostCompositorTeardown(result);
      return nullptr;
    }
    for ( uint8_t i = 0; i < c->num_rings; ++i ) {
      // TODO/FIXME: replace 3 * ... with NeoPixel's numBytes
      c->ghost_buffers[i] = new uint8_t[3 * rings[i]->numLeds()];
      if ( nullptr == c->ghost_buffers[i] ) {
        ghostCompositorTeardown(result);
        return nullptr;
      }
    }
  }
  return result;
}

void ghostCompositorStep(void* ctx) {
  GhostCompositorContext *result = new GhostCompositorContext;
  if(nullptr == result) {
      return;
  }
}

void ghostCompositorAddAnimation(void* ctx, const char* animation_name) {

}

#endif
