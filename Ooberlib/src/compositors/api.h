#pragma once
#ifndef COMPOSITOR_API_H
#define COMPOSITOR_API_H

#include "../animations/api.h"

// fwd decls
struct CompositorWithState;

struct CompositorOption {
  const char *  name;
  const char *  description;
  const int32_t min;
  const int32_t max;
  const int32_t default_value;
};

typedef void *(*compositorSetupFptr)(Adafruit_NeoPixel **, uint8_t num_rings);
typedef void (*compositorTeardownFptr)(void *);
typedef void (*compositorStepFptr)(void *);
typedef void (*compositorSetOptionFptr)(void *,
                                        const char *   option_name,
                                        const uint32_t value);
typedef void (*compositorGetOptionFptr)(const CompositorOption **optionInfo,
                                        size_t *                 n);
typedef void (*compositorAddAnimationFptr)(void *, AnimationWithState *);
typedef void (*compositoraddSubCompositorFptr)(void *, CompositorWithState *);

struct Compositor {
  const char *                   name;
  compositorSetupFptr            setupFunc;
  compositorTeardownFptr         teardownFunc;
  compositorStepFptr             stepFunc;
  compositorAddAnimationFptr     addAnimationFunc;
  compositoraddSubCompositorFptr addSubCompositorFunc;
  compositorSetOptionFptr        setOptFunc;
  compositorGetOptionFptr        getOptionsFunc;
};

Compositor *getCompositorByName(const char *name);
const char *getCompositorName(Compositor *comp);
const char *getCompositorNameFromStepFunc(compositorStepFptr stepFunc);

struct CompositorWithState {
  Compositor *compositor = nullptr;
  void *      ctx        = nullptr;
  explicit CompositorWithState(Compositor *comp, void *c)
      : compositor(comp), ctx(c) {}
  CompositorWithState(const CompositorWithState &that)
      : compositor(that.compositor), ctx(that.ctx) {}
};

#endif
