#pragma once
#ifndef PASSTHROUGH_COMPOSITOR_H
#define PASSTHROUGH_COMPOSITOR_H

#include <cstdint>

#include <Adafruit_NeoPixel.h>

#include "api.h"

void *passthroughCompositorSetup(Adafruit_NeoPixel **rings, uint8_t num_rings);

void passthroughCompositorTeardown(void *ctx);

void passthroughCompositorAddAnimation(void *              ctx,
                                       AnimationWithState *animwstate);

void passthroughCompositorStep(void *ctx);

#endif
