#include "parse_json.h"

#include "animations/api.h"
#include "compositors/api.h"

#include "tmpl_helpers.h"

namespace {
constexpr size_t MAX_ANIMATIONS_IN_SUBCHAIN = 5;
const size_t     default_json_parse_size    = 5 * 1024;

inline long espclock() { return ESP.getCycleCount(); }
const auto  CYCLES_PER_SEC = 80.e6;

/// batch option setters

template<typename C> void set_options(C *item, JsonObject options) {}

template<>
void set_options<CompositorWithState>(CompositorWithState *item,
                                      JsonObject           options) {
  if ( nullptr == item || nullptr == item->compositor || nullptr == item->ctx ||
       nullptr == item->compositor->setOptFunc || !options ) {
    return;
  }
  for ( const auto kv: options ) {
    item->compositor->setOptFunc(item->ctx, kv.key().c_str(),
                                 kv.value().as<uint32_t>());
  }
}
template<>
void set_options<AnimationWithState>(AnimationWithState *item,
                                     JsonObject          options) {
  if ( nullptr == item || nullptr == item->animation || nullptr == item->ctx ||
       nullptr == item->animation->setOptFunc || !options ) {
    return;
  }
  for ( const auto kv: options ) {
    item->animation->setOptFunc(item->ctx, kv.key().c_str(),
                                kv.value().as<uint32_t>());
  }
}

/* add a given comp or anim to root comp */

template<class T> bool add_to_root(CompositorWithState *root, T *) {
  return false;
}

template<>
bool add_to_root(CompositorWithState *root, CompositorWithState *item) {
  if ( root && root->compositor && root->compositor->addSubCompositorFunc ) {
    root->compositor->addSubCompositorFunc(root->ctx, item);
    return true;
  }
  return false;
}

template<>
bool add_to_root(CompositorWithState *root, AnimationWithState *anim) {
  if ( root && root->compositor && root->compositor->addAnimationFunc ) {
    root->compositor->addAnimationFunc(root->ctx, anim);
    return true;
  }
  return false;
}

/* predicates if json object is comp or anim */

inline bool type_is_compositor(const char *type) {
  return (nullptr != type && (!strcmp(type, "c") || !strcmp(type, "comp") ||
                              !strcmp(type, "compositor")));
}

inline bool type_is_animation(const char *type) {
  return (nullptr != type && (!strcmp(type, "a") || !strcmp(type, "anim") ||
                              !strcmp(type, "animation")));
}

inline bool type_is_direct_setting(const char *type) {
  return (nullptr != type && (!strcmp(type, "d") || !strcmp(type, "direct") ||
                              !strcmp(type, "direct_setting")));
}

bool build_ring_effect_single(CompositorWithState **roots,
                              Adafruit_NeoPixel **  rings,
                              const uint8_t         num_rings,
                              JsonObject            obj,
                              bool                  top_level = false) {
  if ( !obj ) {
    return true;
  }

  CompositorWithState *root = nullptr;

  const char *type = obj["type"].as<const char *>();

  // get designated target ring
  uint8_t target_ring_index = 0;

  // immediately handle 'direct setting' json
  if ( type_is_direct_setting(type) ) {
    target_ring_index = obj["for"].as<size_t>();
    if ( 0 < target_ring_index && target_ring_index <= num_rings ) {
      target_ring_index -= 1;
    } else {
      return false;
    }
    uint8_t counter = 0;
    for ( const auto pdata: obj["v"].as<JsonArray>() ) {
      rings[target_ring_index]->setPixelColor(counter++, pdata.as<uint32_t>());
      if ( 45 == counter ) {
        break;
      }
    }
    return true;
  }

  if ( top_level ) {
    if ( nullptr == obj["for"] ) {
      return false;
    }
    target_ring_index = obj["for"].as<size_t>();
    if ( 0 < target_ring_index && target_ring_index <= num_rings ) {
      target_ring_index -= 1;
    } else {
      target_ring_index = 0;
    }
  } else {
    // root = roots[target_ring_index];
    return false;
  }

  if ( type_is_compositor(type) ) {
    const auto comp =
        make_compositor(rings + target_ring_index, 1, obj["name"]);
    const auto options = obj["options"].as<JsonObject>();
    set_options<CompositorWithState>(comp, options);
    if ( top_level ) {
      root = comp;
    } else {
      add_to_root(root, comp);
    }
  } else if ( type_is_animation(type) ) {
    if ( top_level ) {
      // a top-level non-compositor needs an implicit passthrough compositor
      root = make_compositor(rings + target_ring_index, 1, "passthrough");
    }
    const auto anim = make_animation(rings + target_ring_index, 1, obj["name"]);
    add_to_root(root, anim);
    const auto options = obj["options"];
    set_options<AnimationWithState>(anim, options.as<JsonObject>());
  } else {
    return false;
  }

  if ( top_level ) {
    roots[target_ring_index] = root;
  }

  // recursion
  bool res = true, temp;
  if ( obj.containsKey("chain") ) {
    for ( const auto &link: obj["chain"].as<JsonArray>() ) {
      temp = build_ring_effect_single(&root, rings + target_ring_index, 1, link,
                                      false);
      res  = res && temp;
    }
  }

  return res;
}

bool build_ring_effect(CompositorWithState **roots,
                       Adafruit_NeoPixel **  rings,
                       const uint8_t         num_rings,
                       DynamicJsonDocument & doc) {
  // if we have different effects for individual rings...
  if ( doc.is<JsonArray>() ) {
    bool res{true}, temp;
    for ( JsonObject o: doc.as<JsonArray>() ) {
      temp = build_ring_effect_single(roots, rings, num_rings, o, true);
      res  = res && temp;
    }
    return res;
  } else if ( doc.is<JsonObject>() ) {
    auto temp = doc.as<JsonObject>();
    Serial.print(temp);
    Serial.println("\n-----\n");
    Serial.flush();
    return build_ring_effect_single(roots, rings, num_rings, temp, true);
  } else {
    return false;
  }
}
}  // namespace

bool parseJson(CompositorWithState **roots,
               Adafruit_NeoPixel **  rings,
               const uint8_t         num_rings,
               char *                str) {
  const auto mem_before = ESP.getFreeHeap();
  const auto begin      = ::espclock();

  DynamicJsonDocument d(default_json_parse_size);
  const auto          err = deserializeJson(d, str);

  const auto end       = ::espclock();
  const auto mem_after = ESP.getFreeHeap();
  if ( err ) {
    Serial.printf("error parsing json: %s\n", err.c_str());
    return false;
  }
  Serial.printf("mem bytes used %lld\n",
                (long long)(mem_after) - (long long)(mem_before));
  Serial.printf("took %ld cycles = %.3f microseconds\n", end - begin,
                (float)(end - begin) / CYCLES_PER_SEC);

  return build_ring_effect(roots, rings, num_rings, d);
}
