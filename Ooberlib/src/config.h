const char *hostname  = "ooberlights";
const char *wifi_ssid = "your-wifi-ssid-here";
/* leave empty to make it public / unprotected */
const char *wifi_password = "your-wifi-password-here";
/* must be 1, ..., 13 */
const int wifi_channel = 7;
/* this is NOT a security feature. */
const bool wifi_hide_ssid = false;
/* maximum number of devices that can connect.
 * must be 0, ..., 8*/
const int wifi_max_connections = 4;

/* wifi output power in 10ths. must be a multiple of 25
and in range 0 - 20 . */
const uint8_t wifi_output_power_decis = 100;

/* default animation to run when powering on */
const char* default_animation_names[] = {
    "scanline",
    "twinkle",
};

/* "Soft AP" is a feature where a device can be talked to by other devices
   but can also be connected to an actual AP
*/
IPAddress soft_ap_address(192, 168, 1, 127);
IPAddress soft_ap_gateway_address(192, 168, 1, 1);
IPAddress soft_ap_subnet_mask(255, 255, 255, 0);
