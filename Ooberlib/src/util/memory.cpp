#include "memory.h"

#include <inttypes.h>
#include <stdint.h>

#include <Esp.h>

void printMemoryStats(void) {
  const uint32_t free_heap          = ESP.getFreeHeap();
  const uint8_t  heap_frag_pct      = ESP.getHeapFragmentation();
  const uint16_t max_avail_blk_size = ESP.getMaxFreeBlockSize();
  const uint32_t free_stack         = ESP.getFreeContStack();

  Serial.println("===== Memory stats =====");
  Serial.printf("free heap    : %" PRIu32 "\n", free_heap);
  Serial.printf("fragm. heap  : %" PRIu8 " %%\n", heap_frag_pct);
  Serial.printf("max blk size : %" PRIu16 "\n", max_avail_blk_size);
  Serial.println("---");
  Serial.printf("free stack   : %" PRIu32 "\n", free_stack);
  Serial.println("------------------------");
}
