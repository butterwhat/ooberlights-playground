#pragma once
#ifndef UTIL_STRING_H
#define UTIL_STRING_H

#define STRING_BEGINS_WITH(x, y) if ( !strncmp(x, y, strlen(y)) )
#define OR_WITH(x, y)            else if ( !strncmp(x, y, strlen(y)) )

#endif
