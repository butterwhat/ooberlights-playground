#include <stdint.h>
#include <stdlib.h>

/** Encapsulates a stop-watch performance counter
 */
class PerfCounter final {
  uint32_t *slots;
  size_t    num_slots;

public:
  /// create an instance with \p num_slots_ timing slots.
  PerfCounter(const size_t num_slots_)
      : slots(new uint32_t[num_slots_]), num_slots(num_slots_) {}
  /** start timer for the given \p slot_id .
   *
   * \note that this fails silently if the invariant \f$0 \leq \mbox{slot_id} < \mbox{num_slots}\f$ is not met.
   */
  inline PerfCounter &start(const size_t slot_id) {
    if ( nullptr != slots && slot_id < num_slots ) {
      slots[slot_id] = ESP.getCycleCount();
    }
    return *this;
  }
  /** stop timer for the given \p slot_id .
   *
   * @return the elapsed 'time' on valid slot_id, \p UINT32_MAX otherwise.
   *
   * @sa start for the valid \p slot_id invariant.
   */
  inline uint32_t stop(const size_t slot_id) {
    uint32_t temp = ESP.getCycleCount();
    if ( nullptr != slots && slot_id < num_slots ) {
      temp -= slots[slot_id];
      slots[slot_id] = 0;
      return temp;
    }
    return UINT32_MAX;
  }
};
