#pragma once

#define NEW(x)                                                                 \
  new x;                                                                       \
  Serial.printf("NEW %s:%d: %s\n", __FILE__, __LINE__, #x);

#define DELETE(x)                                                              \
  delete x;                                                                    \
  Serial.printf("DELETE %s:%d: %s\n", __FILE__, __LINE__, #x);

void printMemoryStats(void);
