/** generate an RNG seed perusing the few pseudo-random facilities of the ESP.
 *
 * The used sources are:
 *
 *   * MAC address of the WiFi.
 *   * Analog read of an unused pin.
 *   * ESP's own random() call.
 *   * ESP's Chip ID.
 *   * ESP's Flash Chip ID
 *   * Cycle Count since powerup.
 *
 * @return the RNG seed.
 */
uint32_t generateRandomSeed() {
  uint32_t result = UINT32_C(31415926) + ESP.getCycleCount();
  uint32_t mac = 0;
  uint8_t mac_bytes[6];
  WiFi.softAPmacAddress(mac_bytes);
  for (int i = 0; i < 6; ++i) {
    mac <<= 8;
    mac |= mac_bytes[i];
  }

  //add in the unit's mac address
  result ^= mac;

  //add in a hopefully 'random' voltage on the ADC pin
  result ^= analogRead(A0);

  //add in time since boot -- sadly can't use clock() here
  result ^= (uint32_t)(time(NULL) * 256);

  //esp specific pseudo-random parts that seem to be generated from enabled wifi
  result ^= ESP.random();
  //esp specific chip id
  result ^= ESP.getChipId();
  //esp specific flash chip id
  result ^= ESP.getFlashChipId();
  //esp specific cycle count since boot
  result ^= ESP.getCycleCount();

  return result;
}

