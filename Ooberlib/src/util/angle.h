#pragma once
#ifndef UTIL_ANGLE_H
#define UTIL_ANGLE_H

#define OOBERLIGHTS_PROTOTYPE

#ifdef OOBERLIGHTS_PROTOTYPE
// ugly hack for the prototype version of the boards
const uint8_t l24[24] = {42, 43, 44, 21, 22, 23, 24, 25, 26, 27, 28, 29,
                         30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41};
const uint8_t l12[12] = {19, 20, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
const uint8_t l8[8]   = {7, 8, 1, 2, 3, 4, 5, 6};

uint16_t led_for_angle(const uint8_t layer, const int degrees_centi) {
  const uint8_t *arr;
  int            count;
  switch ( layer ) {
  case 0:
    return 0;
    break;
  case 1:
    arr   = l8;
    count = 8;
    break;
  case 2:
    arr   = l12;
    count = 12;
    break;
  case 3:
    arr   = l24;
    count = 24;
    break;
  default:
    return UINT16_MAX;
  }
  const uint16_t inter_led_angle = (360 * 100) / count;
  const int minus_led = (degrees_centi - inter_led_angle / 2) / inter_led_angle;
  const int plus_led  = (degrees_centi + inter_led_angle / 2) / inter_led_angle;
  int       minus_delta = degrees_centi - minus_led * inter_led_angle;
  int       plus_delta  = plus_led * inter_led_angle - degrees_centi;

  if ( minus_delta < 0 ) {
    minus_delta = -minus_delta;
  }
  if ( plus_delta < 0 ) {
    plus_delta = -plus_delta;
  }
  if ( minus_delta < plus_delta ) {
    return arr[(count + minus_led) % count];
  }
  return arr[(count + plus_led) % count];
}
#else
// first led on ring offset in degrees from 'east'. negative means towards
// 'north'
const int16_t initial_angles[] = {0, -45, -30, -45};
// number of leds per ring layer -- center counts as 1
const uint8_t per_layer[] = {1, 8, 12, 24};
// partial sums of per_layer
const uint8_t per_layer_partials[] = {0, 1, 9, 21};

// maps a degree starting at 'north', and going clock-wise, to the nearest led
// on the required layer and returns that led's index in the chain
uint16_t led_for_angle(const uint8_t layer, const int degrees_centi) {
  // garbage input? get an error instead.
  if ( layer >= 4 || degrees_centi < 0 || degrees_centi > 360 * 100 ) {
    return UINT16_MAX;
  }

  // if a layer has at most one led, then any and all angles map to that one.
  if ( 1 >= per_layer[layer] ) {
    return per_layer_partials[layer];
  }

  // same angle we seek, but now zeroed in onto the first led in the given
  // layer.
  int easted_degrees_centi = degrees_centi - (90 + initial_angles[layer]) * 100;
  // if the original angle was f.e. 0, then we might have a negative value here,
  // so let's map that to positive angles only
  easted_degrees_centi += (360 * 100);
  // but now it might also be too large, so let's map it back onto [0,360).
  easted_degrees_centi %= (360 * 100);

  // now map the wanted angle with exactly +/- half of the inter-led angle
  // onto leds, and keep the deltas between those leds and the angle around.
  const int inter_led_angle = (360 * 100) / per_layer[layer];

  int minus_led =
      (easted_degrees_centi - inter_led_angle / 2) / inter_led_angle;
  int plus_led = (easted_degrees_centi + inter_led_angle / 2) / inter_led_angle;
  int minus_delta = easted_degrees_centi - minus_led * inter_led_angle;
  int plus_delta  = plus_led * inter_led_angle - easted_degrees_centi;

  if ( minus_delta < 0 ) {
    minus_delta = -minus_delta;
  }
  if ( plus_delta < 0 ) {
    plus_delta = -plus_delta;
  }

  // now return the led index which has the smaller gap to the desired angle
  if ( minus_delta < plus_delta ) {
    return (uint16_t)(per_layer_partials[layer]) + minus_led;
  }
  // else
  return (uint16_t)(per_layer_partials[layer]) + plus_led;
}
#endif

#undef OOBERLIGHTS_PROTOTYPE

#endif
