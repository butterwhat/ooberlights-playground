#pragma once
#define ARDUINOJSON_DECODE_UNICODE        1
#define ARDUINOJSON_ENABLE_NAN            0
#define ARDUINOJSON_DEFAULT_NESTING_LIMIT 9
#define ARDUINOJSON_ENABLE_ARDUINO_STREAM 0
#define ARDUINOJSON_ENABLE_ARDUINO_STRING 0
#define ARDUINOJSON_ENABLE_COMMENTS       1
#define ARDUINOJSON_ENABLE_INFINITY       0
#define ARDUINOJSON_ENABLE_STD_STREAM     0
#define ARDUINOJSON_ENABLE_STD_STRING     0

#include <ArduinoJson.h>

#include <Adafruit_NeoPixel.h>

#include "compositors/api.h"

struct CompositorWithState;

/** create compositor effect chain(s) from parsing json data.
 *
 * @param[in,out]  roots  Array of pointers that the created effect chains will
 * be placed into.
 * @param[in]      rings  The rings that can be perused.
 * @param[in]      num_rings  The number of rings in the \p rings argument.
 * @param[in]      str    The json data. MUST be editable, i.e. non-const.
 *
 * @return \p true on successful parsing and effect chain creation, \p false
 * otherwise.
 */
bool parseJson(CompositorWithState **roots,
               Adafruit_NeoPixel **  rings,
               const uint8_t         num_rings,
               char *                str);
