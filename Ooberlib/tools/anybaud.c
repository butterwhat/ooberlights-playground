/** Set any baud rate for a serial device on Linux (and possibly POSIX),
 * as GNU stty only permits certain specific baud rates.
 *
 * Found via http://sensornodeinfo.rockingdlabs.com/blog/2016/01/19/baud74880/
 * and taken from https://gist.github.com/sentinelt/3f1a984533556cf890d9
 *
 * Heavily modified to use a more portable API and better error handling /
 * reporting.
 */

#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
    if (3 != argc) {
        printf("%s <device> <speed>\n", (0 < argc) ? argv[0] : "<progname>");
        printf("Set speed for a serial device.\nExample:\n");
        printf("    %s /dev/ttyUSB0 75000\n",
               (0 < argc) ? argv[0] : "<progname>");
        exit(EXIT_FAILURE);
    }

    const int fd = open(argv[1], O_RDONLY);

    if (-1 == fd) {
        perror("open");
        exit(EXIT_FAILURE);
    }

    const unsigned long speed_l = strtoul(argv[2], NULL, 0);
    if (ULONG_MAX == speed_l) {
        perror("strtoul");
        exit(EXIT_FAILURE);
    }

    const speed_t speed = (speed_t)(speed_l);

    if (speed_l != (unsigned long)(speed)) {
        errno = EOVERFLOW;
        perror("speed");
        exit(EXIT_FAILURE);
    }

    struct termios term;
    if (0 != tcgetattr(fd, &term)) {
        perror("tcgetattr");
        exit(EXIT_FAILURE);
    }
#if defined(__GLIBC__) && (defined(_BSD_SOURCE) || defined(_DEFAULT_SOURCE))
    // save ourselves an api call if we can
    if (0 != cfsetspeed(&term, speed)) {
        perror("cfsetspeed");
        exit(EXIT_FAILURE);
    }
#else
    if (0 != cfsetispeed(&term, speed)) {
        perror("cfsetispeed");
        exit(EXIT_FAILURE);
    }
    if (0 != cfsetospeed(&term, speed)) {
        perror("cfsetospeed");
        exit(EXIT_FAILURE);
    }
#endif

    int res;
    do {
        res = close(fd);
    } while (-1 == res && EINTR == errno);

    printf("Changed successfully to %u baud.\n", speed);
    exit(EXIT_SUCCESS);
}
