# Ooberlib JSON format

Ooberlib supports parsing JSON in order to setup animations and compositing.

However, as with all things JSON, the format is of particular importance, and
as such described hereafter.

## Arrays

JSON Arrays are only used in 2 locations:

  1. array of objects for effect chain, and
  2. array of top-level objects.

The latter one's necessary so that individual per ring-set effects can be
specified within a single json file/request.

## Objects

### `type` property

Each JSON object corresponds to an Ooberlib object. The `type` property will
specify whether it is an Animation or a Compositor. Its absence indicates a
set of options for such an Ooberlib object.

#### Example

```json
{
  //...
  "type": "compositor",
  //...
}
```

```json {
  //...
  "type": "animation",
  //...
}
```

To keep things short to write, you can also use `comp` or only `c` for
`compositor`, and `anim` or `a` for `animation`.

### `for` property

Every top-level JSON object is the root for an entire effects chain. The
ring-set it shall be set to is specified by the `for` property, and the value
is a number from 1, 2, ... to the total number of ring sets for a given board.

The `for` property is not used for animations and compositors that are part
of a larger effect chain -- see the effect chain documentation further down
below.

#### Example

```json
{
  "type": "animation",
        //...
  "for" : 2,
  //...
}
```

### `name` property

    For every animation and compositor,
the `name` property specifies the name of the animation or
    compositor to be used.

####Example

```json {
  //...
  "name": "combine",
  //...
}
```

### `options` property

    Each animation or
compositor may allow different options to be set.Those options are set via
                                             the `options` property,
which references a JSON                      object.

#### Example

```json {
  //...
  "options": {
    "color": "#04080c",
    "sweep_hsv": "true",
  }
}
```

### `chain` property

The `chain` property specifies parts of the effect chain beneath the current
compositor. Therefore, `chain` can only be a property for a compositor.

Each element of the chain JSON array is itself a JSON Object which can again
be an animation or compositor.

### Full Example

```json
{
  "for": 1,                          // for ring-set 1
  "type": "comp",                    // create a ...
  "name": "combine",                 // ... "combine" compositor
  "options": {},                     // no options (can be omitted!)
  "chain": [                         // the compositor shall combine
      {
          "type": "animation",       // a scanline animation with
          "name": "scanline",
          "options": {
              "initial_angle": 0     // an initial scanline angle of 0 degrees.
          }
      },
      {
          "type": "animation",       // and a second scanline animation with
          "name": "scanline",
          "options": {
              "initial_angle": 120   // an initial angle of 120 degrees.
          }
      },
      {
          "type": "animation",       // and another scanline with
          "name": "scanline",
          "options": {
              "initial_angle": 240   // an initial angle of 240 degrees.
          }
      }
  ]
}
```

# Hey you... over here...

... you look like a smart guy. Would you like direct access to the LEDs via JSON?

Just set the `type` attribute to one of `d`, `direct`, or `direct_setting`.

Set the `for` attribute as seen before.

Finally, the `v` attribute with an array of 32-bit integers (due to the
limitations of JSON, only decimal is supported, though). The n-th entry of that array will be the color of the n-th LED in that Ring-set.

# Definitions

## Animation

An object that describes what a ring-set shall display and/or how that
display might change over time.

## Compositor

An object that can composit outputs from one to many animations or other
compositors into one.

## Effect Chain

An amalgamation of compositors and animations that work and behave together
as if it were a singular animation.

## Ring-set

A set of rings around a common, although potentially blank, center. Each ring
has LEDs that can be set to an RGB color and a brightness.
