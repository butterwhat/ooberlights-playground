/*
  To upload through terminal you can use: curl -F "image=@firmware.bin"
  esp8266-webupdate.local/update
*/

#undef OOBERDEBUG

#include <Adafruit_NeoPixel.h>
#include "src/arduino_esp8266_includes.h"

#include "src/config.h"
#include "src/animations/api.h"
#include "src/compositors/api.h"
#include "src/util/memory.h"
#include "src/util/perf.h"
#include "src/util/random.h"
#include "src/tmpl_helpers.h"

//#include <GDBStub.h>

static const uint32_t BRIGHTNESS_MASK = 0x2f;
static const uint32_t RGB_BRIGHTNESS_MASK =
    UINT32_C(0x00010101) * BRIGHTNESS_MASK;

static const float        REFRESH_HZ       = 100.f;
static constexpr float    REFRESH_INTERVAL = 1.f / REFRESH_HZ;
static constexpr uint32_t REFRESH_MS       = UINT32_C(1000) * REFRESH_INTERVAL;

#define NUM_PIXELS 45
#define NUM_RINGS  2
Adafruit_NeoPixel *neopixel_rings[NUM_RINGS];
const int          RING_PINS[NUM_RINGS] = {5, 4};

CompositorWithState *comps[NUM_RINGS];
CompositorWithState *comps_next[NUM_RINGS];

ESP8266WebServer *       httpServer;
ESP8266HTTPUpdateServer *httpUpdater;

char *       serial_input_buffer      = nullptr;
const size_t serial_input_buffer_size = 2048;
size_t       serial_input_buffer_pos  = 0;

// important: you need one Ticker per ring
Ticker ringUpdateTickers[NUM_RINGS];

#include "src/parse_json.h"
#include "src/setup_wifi.h"

void setupTickers(void) {
  for ( int i = 0; i < NUM_RINGS; ++i ) {
    ringUpdateTickers[i].attach_ms(
        REFRESH_MS, std::bind(comps[i]->compositor->stepFunc, comps[i]->ctx));
  }
  Serial.println("attached to tickers");
}

void detachTickers(void) {
  for ( int i = 0; i < NUM_RINGS; ++i ) {
    ringUpdateTickers[i].detach();
  }
}

void recursiveDestroy(CompositorWithState *c) {
  if ( nullptr != c && nullptr != c->compositor &&
       nullptr != c->compositor->teardownFunc ) {
    c->compositor->teardownFunc(c->ctx);
  }
  DELETE(c);
}

void switchAnimations(void) {
  CompositorWithState *temp;
  for ( int i = 0; i < NUM_RINGS; ++i ) {
    if ( nullptr != comps[i] && nullptr != comps_next[i] ) {
      temp          = comps[i];
      comps[i]      = comps_next[i];
      comps_next[i] = temp;

      recursiveDestroy(comps_next[i]);
      comps_next[i] = nullptr;
    }
  }
}

void setup(void) {
#include "src/setup_serial.h"
  //gdbstub_init();
#include "src/setup_http.h"

  setupWifi();

  const uint32_t seed = generateRandomSeed();
  Serial.printf("setting seed to %" PRIu32 "\n", seed);
  randomSeed(seed);

  for ( int k = 0; k < NUM_RINGS; ++k ) {
    neopixel_rings[k] =
        NEW(Adafruit_NeoPixel(NUM_PIXELS, RING_PINS[k], NEO_GRB + NEO_KHZ800));
    if ( nullptr != neopixel_rings[k] ) {
      neopixel_rings[k]->begin();
    }
  }

  for ( int i = 0; i < NUM_RINGS; ++i ) {
    comps[i] = make_compositor(neopixel_rings + i, 1, "passthrough");
    if ( nullptr != comps[i] ) {
      AnimationWithState *anim =
          make_animation(neopixel_rings + i, 1, default_animation_names[i]);
      if ( nullptr != comps[i]->compositor->addAnimationFunc ) {
        comps[i]->compositor->addAnimationFunc(comps[i]->ctx, anim);
      }
    }
  }

  Serial.println("animation setup.");
  Serial.flush();

  setupTickers();
}

#define DEBUG_SERIALINPUT_PERF

enum Perfslot {
// enable the defined perf stats
#ifdef DEBUG_SERIALINPUT_PERF
  SLOT_SERIALINPUT_PERF,
#endif
#ifdef DEBUG_HEAP_AND_CYCLES
  SLOT_CYCLES_GENERAL,
#endif

  NUM_PERFSLOTS,

// disable the undefined perf stats
#ifndef DEBUG_SERIALINPUT_PERF
  SLOT_SERIALINPUT_PERF,
#endif
#ifndef DEBUG_HEAP_AND_CYCLES
  SLOT_CYCLES_GENERAL,
#endif
};

PerfCounter perf_counters(NUM_PERFSLOTS);

uint32_t total_ticks = UINT32_C(0);

void listenForSerialInput() {
  // Serial.readStringUntil('\n') is way too slow, so we have to do it the hard
  // way.
  static char c = 0;
#ifdef DEBUG_SERIALINPUT_PERF
  static size_t old_pos = 0;
  old_pos               = serial_input_buffer_pos;
#endif

  perf_counters.start(SLOT_SERIALINPUT_PERF);

  while ( 0 < Serial.available() &&
          serial_input_buffer_pos < serial_input_buffer_size ) {
    serial_input_buffer[serial_input_buffer_pos] = (c = (char)(Serial.read()));

    // the following makes the '\n' == c condition unlikely so in theory a
    // speedup.
    if ( '\n' != c ) {
      ++serial_input_buffer_pos;
    } else {
      break;
    }
  }
#ifdef DEBUG_SERIALINPUT_PERF
  if ( old_pos != serial_input_buffer_pos ) {
    total_ticks += perf_counters.stop(SLOT_SERIALINPUT_PERF);
  }
#endif
  if ( '\n' == serial_input_buffer[serial_input_buffer_pos] ) {
#ifdef DEBUG_SERIALINPUT_PERF
    Serial.printf("%" PRIu32 " ticks\n", total_ticks);
    total_ticks = 0;
#endif
    Serial.printf("command len = %d\n", serial_input_buffer_pos);
    serial_input_buffer[serial_input_buffer_pos] = '\0';
    Serial.printf("command is '%s'\n", serial_input_buffer);
    serial_input_buffer_pos = 0;
    // parseJson(serial_input_buffer);
  }
}

uint16_t counter = 0;

void loop(void) {
  httpServer->handleClient();
  listenForSerialInput();
  MDNS.update();
  if ( !counter++ ) {
    printMemoryStats();
  }
  counter %= 10000;
  delay(1000 * REFRESH_INTERVAL);
}

void handleHttpIndex() {
  if ( httpServer->method() == HTTP_GET ) {
    httpServer->send(200, "text/plain", "index");
  } else if ( httpServer->method() == HTTP_POST ) {
    char buffer[600];  // TODO FIXME ugly hack
    httpServer->arg("plain").toCharArray(buffer, 599);
    buffer[599] = '\0';
    printMemoryStats();
    const bool res = parseJson(comps_next, neopixel_rings, NUM_RINGS, buffer);
    printMemoryStats();
    if ( res ) {
      detachTickers();
      switchAnimations();
      setupTickers();
    }
    httpServer->send(200, "text/plain", res ? "valid json" : "invalid json");
  }
}

void replyOk() { httpServer->send(200, "text/plain", ""); }

/// json api
void handleHttpLedStatus() {
  // char buffer[64];
  // snprintf(buffer, 63, "%" PRIu32 " %lu", counter, (unsigned
  // long)time(NULL));
  replyOk();
}
void handleHttpLeds() {
  // TODO
}

void doNothing() {
  // literally nothing
}
